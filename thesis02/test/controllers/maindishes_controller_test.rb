require 'test_helper'

class MaindishesControllerTest < ActionController::TestCase
  setup do
    @maindish = maindishes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:maindishes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create maindish" do
    assert_difference('Maindish.count') do
      post :create, maindish: { name: @maindish.name, price: @maindish.price }
    end

    assert_redirected_to maindish_path(assigns(:maindish))
  end

  test "should show maindish" do
    get :show, id: @maindish
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @maindish
    assert_response :success
  end

  test "should update maindish" do
    patch :update, id: @maindish, maindish: { name: @maindish.name, price: @maindish.price }
    assert_redirected_to maindish_path(assigns(:maindish))
  end

  test "should destroy maindish" do
    assert_difference('Maindish.count', -1) do
      delete :destroy, id: @maindish
    end

    assert_redirected_to maindishes_path
  end
end
