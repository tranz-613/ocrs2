class CreateDishes < ActiveRecord::Migration
  def change
    create_table :dishes do |t|
      t.string :name
      t.decimal :price
      t.references :Menu_Type, index: true
      t.references :Dish_Type, index: true

      t.timestamps null: false
    end
    add_foreign_key :dishes, :Menu_Types
    add_foreign_key :dishes, :Dish_Types
  end
end
