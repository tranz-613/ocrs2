json.array!(@cocktails) do |cocktail|
  json.extract! cocktail, :id, :name, :price
  json.url cocktail_url(cocktail, format: :json)
end
