json.array!(@maindishes) do |maindish|
  json.extract! maindish, :id, :name, :price
  json.url maindish_url(maindish, format: :json)
end
