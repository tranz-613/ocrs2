class CreateMaindishes < ActiveRecord::Migration
  def change
    create_table :maindishes do |t|
      t.string :name
      t.decimal :price

      t.timestamps null: false
    end
  end
end
