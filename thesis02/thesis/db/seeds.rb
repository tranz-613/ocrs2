# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

bt = MenuType.create! :name => "Buffet Type"
# pm = MenuType.create! :name => "Packed Meal"
# ct = MenuType.create! :name => "Cocktail Type"

apetizer = Dish_Type.create! :name => "Apetizer"
# maindish = DishType.create! :name => "Main Dish"
# dessert = DishType.create! :name => "Dessert"
# rice = DishType.create! :name => "Rice"
# oc = DishType.create! :name => "Optional Charges"

Dish.create! :name => "The Hunger Games", :price => 20.00, :MenuType => bt, :DishType => apetizer

# Dish.create! :name => "Fresh Lumpia", :price => 200.00, :menu_type => bt, :dish_type => apetizer
# Dish.create! :name => "Chicken Macaroni Salad", :price => 200.00, :menu_type => bt, :dish_type => apetizer
# Dish.create! :name => "Mixed Vegetable Salad", :price => 200.00, :menu_type => bt, :dish_type => apetizer
# Dish.create! :name => "Potato Apple Salad", :price => 200.00, :menu_type => bt, :dish_type => apetizer
# Dish.create! :name => "Shell Macaroni Salad", :price => 200.00, :menu_type => bt, :dish_type => apetizer
# Dish.create! :name => "Ubod Pineapple Salad", :price => 200.00, :menu_type => bt, :dish_type => apetizer
#
# Dish.create! :name => "Braised Pork", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Grilled Pork", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Pork Asado", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Pork Menudo", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Shredded Pork", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Stir Fry Pork", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Sweet and Sour Pork", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Pork Ribs in Barbecue Sauce", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Cabbage Rolls", :price => 200.00, :menu_type => bt, :dish_type => maindish
#
# Dish.create! :name => "Buttered Chicken", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Chicken Chunk with Honey", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Chicken Lolipop", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Chicken Pandan", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Chicken Adobo", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Chicken Relleno", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Chicken Roll w/ Ham and Butter", :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Chicken Steak Fillet", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Lemon Chicken", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Chicken Boxing Wings", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Grilled Chicken Teriyaki", :price => 200.00, :menu_type => bt, :dish_type => maindish
#
# Dish.create! :name => "Fish Fillet w/ Chilli Sauce", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Fish FIllet w/ Corn Sauce", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Fish Fillet w/ Tartar Sauce", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Fish Roll with Bacan & Cheese", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Shrimp Balls", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Shrimp Rolls", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Steamed Fish Fillet w/ Ham & Black Mushroom", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Steamed Fish w/ Mayo", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Fish Fillet in Chili Bean Paste Sauce", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Sweet and Sour Fish", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Steamed Fish Fillet w/ Lemon & Butter Sauce", :price => 200.00, :menu_type => bt, :dish_type => maindish
#
# Dish.create! :name => "Beef Caldereta", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Beef Tips in Oyster Sauce", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Beef w/ Broccoli", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Beef w/ Mashed Potatoes", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Beef w/ Mushroom", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Beef w/ Onions", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Braised Beef or Canto Callos", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Mango Beef", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Stir Fry Beef", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Lengua Estofado", :price => 200.00, :menu_type => bt, :dish_type => maindish
#
# Dish.create! :name => "Baked Macaroni", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Bam-i Guisado", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Beef Lasagna", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Chicken Lasagna", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Pasta Carbonara", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Chow Mien", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Fettuccini", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Beef Stroganoff", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Seafood Casserole", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Sotanghon Guissado", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Spaghetti", :price => 200.00, :menu_type => bt, :dish_type => maindish
#
# Dish.create! :name => "Chao Pat Chin", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Four Seasons Vegetables", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Sauteed Vegetables", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Three Kinds of Mushrooms", :price => 200.00, :menu_type => bt, :dish_type => maindish
# Dish.create! :name => "Calderetang Kambing", :price => 200.00, :menu_type => bt, :dish_type => maindish
#
#
# Dish.create! :name => "Tropical Fresh Fruits", :price => 200.00, :menu_type => bt, :dish_type => dessert
# Dish.create! :name => "Mango Pandan Salad", :price => 200.00, :menu_type => bt, :dish_type => dessert
# Dish.create! :name => "Mango Pandan Gelatin", :price => 200.00, :menu_type => bt, :dish_type => dessert
# Dish.create! :name => "Mango Mousse", :price => 200.00, :menu_type => bt, :dish_type => dessert
# Dish.create! :name => "Fruit Salad", :price => 200.00, :menu_type => bt, :dish_type => dessert
# Dish.create! :name => "Black Sambo", :price => 200.00, :menu_type => bt, :dish_type => dessert
# Dish.create! :name => "Leche Flan", :price => 200.00, :menu_type => bt, :dish_type => dessert
# Dish.create! :name => "Maja Blanca", :price => 200.00, :menu_type => bt, :dish_type => dessert
# Dish.create! :name => "Petche Petche", :price => 200.00, :menu_type => bt, :dish_type => dessert
# Dish.create! :name => "Custard Cake", :price => 200.00, :menu_type => bt, :dish_type => dessert
# Dish.create! :name => "Cassava Cake", :price => 200.00, :menu_type => bt, :dish_type => dessert
#
# Dish.create! :name => "Plain Rice", :price => 200.00, :menu_type => bt, :dish_type => rice
# Dish.create! :name => "Shanghai Rice", :price => 200.00, :menu_type => bt, :dish_type => rice
# Dish.create! :name => "Garlic Rice", :price => 200.00, :menu_type => bt, :dish_type => rice
#
# Dish.create! :name => "Tents for Reception", :price => 200.00, :menu_type => bt, :dish_type => oc
# Dish.create! :name => "Red Carpet", :price => 200.00, :menu_type => bt, :dish_type => oc
# Dish.create! :name => "Ceiling Decor", :price => 200.00, :menu_type => bt, :dish_type => oc
# Dish.create! :name => "Cocktail Table", :price => 200.00, :menu_type => bt, :dish_type => oc
# Dish.create! :name => "Tiffany Chairs", :price => 200.00, :menu_type => bt, :dish_type => oc
# Dish.create! :name => "Wine Glasses", :price => 200.00, :menu_type => bt, :dish_type => oc
# Dish.create! :name => "Floral Arrangements", :price => 200.00, :menu_type => bt, :dish_type => oc
# Dish.create! :name => "Kid's Tables and Chairs", :price => 200.00, :menu_type => bt, :dish_type => oc
#
#
# #Packed Meal
#
# Dish.create! :name => "Braised Pork", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Grilled Pork", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Pork Asado", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Pork Menudo", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Shredded Pork", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Stir Fry Pork", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Sweet and Sour Pork", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Pork Ribs in Barbecue Sauce", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Cabbage Rolls", :price => 200.00, :menu_type => maindish, :dish_type => pm
#
# Dish.create! :name => "Buttered Chicken", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Chicken Chunk with Honey", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Chicken Lolipop", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Chicken Pandan", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Chicken Adobo", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Chicken Relleno", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Chicken Roll w/ Ham and Butter", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Chicken Steak Fillet", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Fried Chicken with Sesame Seeds & Gravy", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Lemon Chicken", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Chicken Boxing Wings", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Grilled Chicken Teriyaki", :price => 200.00, :menu_type => maindish, :dish_type => pm
#
# Dish.create! :name => "Fish Fillet w/ Chilli Sauce", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Fish FIllet w/ Corn Sauce", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Fish Fillet w/ Tartar Sauce", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Fish Roll with Bacan & Cheese", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Shrimp Balls", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Shrimp Rolls", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Steamed Fish Fillet w/ Ham & Black Mushroom", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Steamed Fish w/ Mayo", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Fish Fillet in Chili Bean Paste Sauce", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Sweet and Sour Fish", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Steamed Fish Fillet w/ Lemon & Butter Sauce", :price => 200.00, :menu_type => maindish, :dish_type => pm
#
# Dish.create! :name => "Beef Caldereta", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Beef Tips in Oyster Sauce", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Beef w/ Broccoli", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Beef w/ Mashed Potatoes", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Beef w/ Mushroom", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Beef w/ Onions", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Braised Beef or Canto Callos", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Mango Beef", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Stir Fry Beef", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Lengua Estofado", :price => 200.00, :menu_type => maindish, :dish_type => pm
#
# Dish.create! :name => "Baked Macaroni", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Bam-i Guisado", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Beef Lasagna", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Chicken Lasagna", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Pasta Carbonara", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Chow Mien", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Fettuccini", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Beef Stroganoff", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Seafood Casserole", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Sotanghon Guissado", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Spaghetti", :price => 200.00, :menu_type => maindish, :dish_type => pm
#
# Dish.create! :name => "Chao Pat Chin", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Four Seasons Vegetables", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Sauteed Vegetables", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Three Kinds of Mushrooms", :price => 200.00, :menu_type => maindish, :dish_type => pm
# Dish.create! :name => "Calderetang Kambing", :price => 200.00, :menu_type => maindish, :dish_type => pm
#
#
# Dish.create! :name => "Tropical Fresh Fruits", :price => 200.00, :menu_type => dessert, :dish_type => pm
# Dish.create! :name => "Mango Pandan Salad", :price => 200.00, :menu_type => dessert, :dish_type => pm
# Dish.create! :name => "Mango Pandan Gelatin", :price => 200.00, :menu_type => dessert, :dish_type => pm
# Dish.create! :name => "Mango Mousse", :price => 200.00, :menu_type => dessert, :dish_type => pm
# Dish.create! :name => "Fruit Salad", :price => 200.00, :menu_type => dessert, :dish_type => pm
# Dish.create! :name => "Black Sambo", :price => 200.00, :menu_type => dessert, :dish_type => pm
# Dish.create! :name => "Leche Flan", :price => 200.00, :menu_type => dessert, :dish_type => pm
# Dish.create! :name => "Maja Blanca", :price => 200.00, :menu_type => dessert, :dish_type => pm
# Dish.create! :name => "Petche Petche", :price => 200.00, :menu_type => dessert, :dish_type => pm
# Dish.create! :name => "Custard Cake", :price => 200.00, :menu_type => dessert, :dish_type => pm
# Dish.create! :name => "Cassava Cake", :price => 200.00, :menu_type => dessert, :dish_type => pm
#
# Dish.create! :name => "Plain Rice", :price => 200.00, :menu_type => rice, :dish_type => pm
#
#
# #Cocktail Type
#
#
# Dish.create! :name => "Braised Pork", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Grilled Pork", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Pork Asado", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Pork Menudo", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Shredded Pork", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Stir Fry Pork", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Sweet and Sour Pork", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Pork Ribs in Barbecue Sauce", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Cabbage Rolls", :price => 200.00, :menu_type => maindish, :dish_type => ct
#
# Dish.create! :name => "Buttered Chicken", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Chicken Chunk with Honey", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Chicken Lolipop", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Chicken Pandan", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Chicken Adobo", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Chicken Relleno", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Chicken Roll w/ Ham and Butter", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Chicken Steak Fillet", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Fried Chicken with Sesame Seeds & Gravy", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Lemon Chicken", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Chicken Boxing Wings", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Grilled Chicken Teriyaki", :price => 200.00, :menu_type => maindish, :dish_type => ct
#
# Dish.create! :name => "Fish Fillet w/ Chilli Sauce", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Fish FIllet w/ Corn Sauce", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Fish Fillet w/ Tartar Sauce", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Fish Roll with Bacan & Cheese", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Shrimp Balls", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Shrimp Rolls", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Steamed Fish Fillet w/ Ham & Black Mushroom", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Steamed Fish w/ Mayo", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Fish Fillet in Chili Bean Paste Sauce", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Sweet and Sour Fish", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Steamed Fish Fillet w/ Lemon & Butter Sauce", :price => 200.00, :menu_type => maindish, :dish_type => ct
#
# Dish.create! :name => "Beef Caldereta", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Beef Tips in Oyster Sauce", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Beef w/ Broccoli", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Beef w/ Mashed Potatoes", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Beef w/ Mushroom", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Beef w/ Onions", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Braised Beef or Canto Callos", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Mango Beef", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Stir Fry Beef", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Lengua Estofado", :price => 200.00, :menu_type => maindish, :dish_type => ct
#
# Dish.create! :name => "Baked Macaroni", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Bam-i Guisado", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Beef Lasagna", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Chicken Lasagna", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Pasta Carbonara", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Chow Mien", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Fettuccini", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Beef Stroganoff", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Seafood Casserole", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Sotanghon Guissado", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Spaghetti", :price => 200.00, :menu_type => maindish, :dish_type => ct
#
# Dish.create! :name => "Chao Pat Chin", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Four Seasons Vegetables", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Sauteed Vegetables", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Three Kinds of Mushrooms", :price => 200.00, :menu_type => maindish, :dish_type => ct
# Dish.create! :name => "Calderetang Kambing", :price => 200.00, :menu_type => maindish, :dish_type => ct
#
#
# Dish.create! :name => "Tropical Fresh Fruits", :price => 200.00, :menu_type => dessert, :dish_type => ct
# Dish.create! :name => "Mango Pandan Salad", :price => 200.00, :menu_type => dessert, :dish_type => ct
# Dish.create! :name => "Mango Pandan Gelatin", :price => 200.00, :menu_type => dessert, :dish_type => ct
# Dish.create! :name => "Mango Mousse", :price => 200.00, :menu_type => dessert, :dish_type => ct
# Dish.create! :name => "Fruit Salad", :price => 200.00, :menu_type => dessert, :dish_type => ct
# Dish.create! :name => "Black Sambo", :price => 200.00, :menu_type => dessert, :dish_type => ct
# Dish.create! :name => "Leche Flan", :price => 200.00, :menu_type => dessert, :dish_type => ct
# Dish.create! :name => "Maja Blanca", :price => 200.00, :menu_type => dessert, :dish_type => ct
# Dish.create! :name => "Petche Petche", :price => 200.00, :menu_type => dessert, :dish_type => ct
# Dish.create! :name => "Custard Cake", :price => 200.00, :menu_type => dessert, :dish_type => ct
# Dish.create! :name => "Cassava Cake", :price => 200.00, :menu_type => dessert, :dish_type => ct
#
# Dish.create! :name => "Plain Rice", :price => 200.00, :menu_type => rice, :dish_type => ct
#





